# k8s

## Docker  
Test de la aplicacion en docker:  
***`docker run -p 8091:8091 diegoazd/hello-api`***

ejecutar:  
curl ***`http://localhost:8091/hello`***  

## configuracion K8
`gcloud init`  (selecionar zona us-central1-a)  
`gcloud container clusters get-credentials k8s-learning`  
`kubectl get pods`

1. Deployment con kubectl  
⋅⋅* `kubectl run {name} --image=diegoazd/hello-api:latest --replicas=1`  
⋅⋅* `kubectl get deployments`  
⋅⋅* `kubectl describe deployment {name}` 
⋅⋅* `kubectl expose deployment {name} --type=LoadBalancer --port 80 --target-port 8091`  
⋅⋅* `kubectl get svc`  
⋅⋅* `kubectl edit deployment ${name}` (Cambiar numero de replicas)  
⋅⋅* Borrar el servicio `kubectl delete svc {serviceName}`  
⋅⋅* Liga nuestro servicio `kubectl port-forward ${podName} 8091:8091`  

2. Deployment con yaml  
⋅⋅* `editar deployment.yml` (Reemplazar {name})  
⋅⋅* `kubectl apply -f deployment.yml`  
⋅⋅* `kubectl get deployments`  
⋅⋅* Actualizar a 2 las replicas de deployment.yml y ejecutar `kubectl apply -f deployment.yml`  
⋅⋅* Buscar los pods con `kubectl get pods -o=name | grep {name}`  
⋅⋅* `editar svc.yml`
⋅⋅* `kubectl apply -f svc.yml`
⋅⋅* `kubectl get svc`

3. Cluster IP  
⋅⋅* `editar clusterIp.yml`  
⋅⋅* `kubectl apply -f clusterIp.yml`  
⋅⋅* `kubectl get svc`  
⋅⋅* `curl {clusterIp}:8091/hello` que paso?  
⋅⋅* `kubectl exec -it ubuntu /bin/bash`  
⋅⋅* `curl {clusterIp}:8091/hello`  

4. Node Port  
⋅⋅* `editar nodePort.yml`  (Puertos validos: 30000-32767)  
⋅⋅* `kubectl apply -f nodePort.yml`  
⋅⋅* `kubectl get svc`  
⋅⋅* `kubectl exec -it ubuntu /bin/bash`  
⋅⋅* `curl {clusterIp}:8091/hello` que paso?  
⋅⋅* `Ver ips de los nodos`  
⋅⋅* `curl {nodeIp}:{nodePort}/hello` 

5. Liveness y Readiness probes  
⋅⋅* `editar liveness.yml`  
⋅⋅* `kubectl apply -f liveness.yml`  
⋅⋅* `editar readinessProbe por /health, hacer deployment` Ver por un minuto los pods, que paso?  
⋅⋅* `regresar readinessProbe a /hello y editar livenessProbe a /health, hacer deployment` Ver por un minuto los pods, que paso?  