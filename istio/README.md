# Istio  

## Traffic management  
  
1. Request routing  
⋅⋅* `Ver el archivo gateway.yml`  
⋅⋅* `Editar archivo deployment.yml`  
⋅⋅* `kubectl apply -f deployment.yml`  
Como exponemos el servicio sin un load balancer?  
⋅⋅* `Editar el archivo virtual-service.yml`  
⋅⋅* `kubectl apply -f virtual-service.yml`  
  
2. Traffic shifting  
⋅⋅* `Editar archivo deployment23.yml`  
⋅⋅* `kubectl apply -f deployment23.yml`  
⋅⋅* Que pasa si hacemos peticiones a la url?  
⋅⋅* `Editar archivo routing.yml`  
⋅⋅* `kubectl apply -f routing.yml`  
⋅⋅* Que pasa si hacemos peticiones a la url?  
  
3. Fault injection
⋅⋅* `Editar archivo fault-injection.yml`  
⋅⋅* `kubectl apply -f fault-injection.yml`  
⋅⋅* Que pasa con las peticiones que regresan error?  
  
## Borrar deployment, service, virtual service y destination rule

4. Circuit breaker  
⋅⋅* `Editar archivo deployment-circuit-breaker.yml`  
⋅⋅* `kubectl apply -f deployment-circuit-breaker.yml`   
⋅⋅* `kubectl apply -f virtual-service.yml`  
⋅⋅* `curl a la aplicacion` todo normal?  
⋅⋅* `Editar archivo ds-circuit-breaker.yml`    
⋅⋅* `kubectl apply -f ds-circuit-breaker.yml`    
### Mandar carga a la aplicacion  
⋅⋅* `fortio load -c 1  -n 20 -loglevel Warning http://35.192.131.223/{name}/hello`  
